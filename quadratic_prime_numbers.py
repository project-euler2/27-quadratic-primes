import math
import time

start_time = time.time()
def is_prime(n):
	if n <= 1:
		return False
	if n == 2:
		return True
	if n > 2 and n % 2 == 0:
		return False
	max_div = math.floor(math.sqrt(n))
	for i in range(3, 1 + max_div, 2):
		if n % i == 0:
			return False
	return True

def Prime_Number_In_Range(n):
	prime = [True for i in range(n+1)]
	prime_values = []
	p = 2
	while(p * p <= n):
		if (prime[p] == True):# If prime[p] is not changed, then it is a prime
			for i in range(p * p, n + 1, p): # Update all multiples of p
				prime[i] = False
		p += 1
	for j in range(2,n):
		if prime[j]:
			prime_values.append(j)
	return prime_values

def longest_quadratic_prime_expressions(limit_a,limit_b):
	negative_b = [prime * -1 for prime in Prime_Number_In_Range(limit_b)]
	positive_b = Prime_Number_In_Range(limit_b)
	b_number = negative_b + positive_b
	max_num_consecutive_prime = 0
	multiplication_solution = 0
	for b in b_number:
		for a in range(limit_a * -1, limit_a):
			consecutive = 0
			i = 0
			while is_prime(i**2 + a * i + b):
				consecutive +=1
				i+= 1
			if consecutive > max_num_consecutive_prime:
				max_num_consecutive_prime = consecutive
				multiplication_solution = a * b

	return multiplication_solution

print(longest_quadratic_prime_expressions(1000,1000))
print(f"--- {(time.time() - start_time):.2f} seconds ---"  )